# frozen_string_literal: true

require "logger"

describe Gitlab::QA::Support::GitlabVersionInfo do
  subject(:previous_version) { described_class.new(current_version, edition).previous_version(semver_component) }

  let(:edition) { "ee" }
  let(:tags) do
    <<~JSON
      [
        {"layer": "", "name": "latest"},
        {"layer": "", "name": "14.8.0-ee.0"},
        {"layer": "", "name": "14.9.5-ee.0"},
        {"layer": "", "name": "15.1.0-ee.0"},
        {"layer": "", "name": "15.2.1-ee.0"}
      ]
    JSON
  end

  let(:page2_tags) do
    <<~JSON
      [
        {"layer": "", "name": "14.5.0-ee.0"},
        {"layer": "", "name": "14.0.0-ee.0"},
        {"layer": "", "name": "13.12.0-ee.0"},
        {"layer": "", "name": "13.5.1-ee.0"}
      ]
    JSON
  end

  before do
    allow(Gitlab::QA::Runtime::Logger).to receive(:logger) { Logger.new(StringIO.new) }

    stub_request(:get, "https://registry.hub.docker.com/v2/namespaces/gitlab/repositories/gitlab-ee/tags?page=1&page_size=100")
      .with(body: "{}")
      .to_return(status: code, body: %({ "results": #{tags}, "next": "some url"}))
    stub_request(:get, "https://registry.hub.docker.com/v2/namespaces/gitlab/repositories/gitlab-ee/tags?page=2&page_size=100")
      .with(body: "{}")
      .to_return(status: code, body: %({ "results": #{page2_tags}, "next": null}))
  end

  context 'with current version of 15.3.0-pre' do
    let(:current_version) { '15.3.0-pre' }

    [
      { semver: "major", latest: Gem::Version.new("14.9.5"), latest_fallback: Gem::Version.new("14.0.0") },
      { semver: "minor", latest: Gem::Version.new("15.2.1"), latest_fallback: Gem::Version.new("15.2.0") },
      { semver: "patch", latest: Gem::Version.new("15.2.1"), latest_fallback: Gem::Version.new("15.2.0") }
    ].each do |params|
      context "with previous major version" do
        let(:semver_component) { params[:semver] }

        context "with successful response" do
          let(:code) { 200 }

          it "returns correct previous latest major version" do
            expect(previous_version).to eq(params[:latest])
          end
        end

        context "with unsuccessful response" do
          let(:code) { 500 }

          it "returns correct previous fallback major version" do
            expect(previous_version).to eq(params[:latest_fallback])
          end
        end
      end
    end
  end

  context 'with current version of 14.6.0' do
    let(:current_version) { '14.6.0' }

    [
      { semver: "major", latest: Gem::Version.new("13.12.0"), latest_fallback: Gem::Version.new("13.0.0") },
      { semver: "minor", latest: Gem::Version.new("14.5.0"), latest_fallback: Gem::Version.new("14.5.0") },
      { semver: "patch", latest: Gem::Version.new("14.5.0"), latest_fallback: Gem::Version.new("14.5.0") }
    ].each do |params|
      context "with previous major version" do
        let(:semver_component) { params[:semver] }

        context "with successful response" do
          let(:code) { 200 }

          it "returns correct previous latest major version" do
            expect(previous_version).to eq(params[:latest])
          end
        end

        context "with unsuccessful response" do
          let(:code) { 500 }

          it "returns correct previous fallback major version" do
            expect(previous_version).to eq(params[:latest_fallback])
          end
        end
      end
    end
  end
end
