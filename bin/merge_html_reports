#!/usr/bin/env ruby

require 'nokogiri'

main_report_file = ARGV.shift
unless main_report_file
  puts 'usage: merge_html_reports <main-report> <base-artifact-url> [parallel reports...]'
  exit 1
end

base_artifact_url = ARGV.shift
unless base_artifact_url
  puts 'usage: merge_html_reports <main-report> <base-artifact-url> [parallel reports...]'
  exit 1
end

# Create the base report with body tag (empty except for the onload method)
new_report = Nokogiri::HTML.parse(File.read(ARGV[0]))
new_report.at_css('body').remove
body = Nokogiri::XML::Node.new('body', new_report)
body[:onload] = "apply_filters()"
new_report.at_css('head').add_next_sibling(body)

ARGV.each do |report_file|
  report = Nokogiri::HTML.parse(File.read(report_file))

  report.css('a').each do |link|
    link_suffix = link['href'].slice(19..-1)
    link['href'] = base_artifact_url + link_suffix
  end

  header = report.css('div #rspec-header')
  tests = report.css('dt[id^="example_group_"]')

  header.search("//h1/text()").first.content = "Test results for job: #{report_file.slice(/rspec-(.*)\.htm/, 1)}"
  header.css("#display-filters input").each do |filter|
    filter.remove_attribute('checked') if filter[:id] != "failed_checkbox"
  end

  tests.each do |test|
    title = test.parent
    group = title.parent
    script = title.css('script')

    if script.inner_html.include? 'makeYellow'
      test.remove_class('passed')
      test.add_class('not_implemented')

      group.remove_class('passed')
      group.add_class('not_implemented')
      header.add_class('not_implemented')

      script.remove
      test.next_sibling.remove
      test.next_sibling.remove

    elsif script.inner_html.include? 'makeRed'
      test.remove_class('passed')
      test.add_class('failed')

      group.remove_class('passed')
      group.add_class('failed')
      header.add_class('failed')

      script.remove
      test.next_sibling.remove
      test.next_sibling.remove
    end
  end

  duration = report.at_css('p#duration')
  totals = report.at_css('p#totals')

  # Also capture results from empty tests for awareness
  results_loc = tests.empty? ? 'div.rspec-report script' : 'div.results script'

  duration_script = report.css(results_loc)[-2]
  totals_script = report.css(results_loc)[-1]

  duration_text = duration_script.text.slice(49..-3)
  totals_text = totals_script.text.slice(47..-3)

  duration.inner_html = duration_text
  totals.inner_html = totals_text

  duration_script.remove
  totals_script.remove

  # Add the new result after the last one to keep the test order
  new_report.css('body')[-1].add_next_sibling(report.at_css('body'))
end

File.write(main_report_file, new_report)
